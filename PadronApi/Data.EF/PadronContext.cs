﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.EF
{
    public class PadronContext : DbContext
    {
        protected PadronContext():base("name=PadronDbConnectionString")
        {
        }
    }
}
