﻿using System.Net.Http.Headers;
using System.Web.Http;
using Newtonsoft.Json.Serialization;

namespace WebAPi
{
    public static class WebAPiConfig
    {
        public static HttpConfiguration Register()
        {

            var config = new HttpConfiguration();


            config.SuppressDefaultHostAuthentication();
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
          

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                                        name: "Cedulados",
                                        routeTemplate: "api/padron/{cedula}",
                                        defaults: new { controller = "Cedulados", cedula = RouteParameter.Optional }
                                      );

            return config;

        }
    }
}
