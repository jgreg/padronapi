﻿using System.Linq;
using System.Web.Http;
using WebAPi.Data.EF;

namespace WebAPi.Controllers
{
    public class CeduladosController : ApiController
    {
        public IHttpActionResult Get(string cedula)
        {
            var db = new PadronContext();

            var cedulados = db.Cedulados.Take(10).ToList();

            return Ok(cedulados);

        }
    }
}
