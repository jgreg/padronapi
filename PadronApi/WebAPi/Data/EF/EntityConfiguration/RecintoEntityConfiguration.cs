using System.Data.Entity.ModelConfiguration;
using WebAPi.Domain;

namespace WebAPi.Data.EF.EntityConfiguration
{
    public class RecintoEntityConfiguration : EntityTypeConfiguration<Recinto>
    {
        public RecintoEntityConfiguration()
        {
            ToTable("Recintos");
            HasKey(r => new {r.RecintoId, r.MunicipioId});

            Property(r => r.RecintoId).HasColumnName("COD_RECINTO").HasMaxLength(5);
            Property(r => r.MunicipioId).HasColumnName("COD_MUNICIPIO").HasMaxLength(3);
            Property(r => r.Nombre).HasColumnName("DESCRIPCION").HasMaxLength(60);
            Property(r => r.Direccion).HasMaxLength(60);
            Property(r => r.Capacidad).HasColumnName("CAPACIDAD_RECINTO").IsOptional();

        }
    }
}