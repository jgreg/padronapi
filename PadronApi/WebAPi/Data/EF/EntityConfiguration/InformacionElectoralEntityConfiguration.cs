using System.Data.Entity.ModelConfiguration;
using WebAPi.Domain;

namespace WebAPi.Data.EF.EntityConfiguration
{
    public class InformacionElectoralEntityConfiguration : EntityTypeConfiguration<InformacionElectoral>
    {
        public InformacionElectoralEntityConfiguration()
        {

            ToTable("Cedulados");
            HasKey(c => new { c.SerieIdent, c.NumIdent, c.DVIdent });
            Property(ie => ie.SerieIdent).HasMaxLength(3).HasColumnName("MUN_CED");
            Property(ie => ie.NumIdent).HasMaxLength(7).HasColumnName("SEQ_CED");
            Property(ie => ie.DVIdent).HasMaxLength(1).HasColumnName("VER_CED");

            Property(ie => ie.MunicipioId).HasColumnName("Cod_Municipio_Electoral").IsOptional();
            Property(ie => ie.RecintoId).HasColumnName("Cod_Recinto").HasMaxLength(5).IsOptional();
            Property(ie => ie.ColegioElectoralId).HasColumnName("Colegio").HasMaxLength(6).IsOptional();

            HasOptional(ie => ie.ColegioElectoral);
            HasOptional(ie => ie.Municipio);
            HasOptional(ie => ie.Recinto);
        }
    }
}