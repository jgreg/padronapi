using System.Data.Entity.ModelConfiguration;
using WebAPi.Domain;

namespace WebAPi.Data.EF.EntityConfiguration
{
    public class CeduladoEntityConfiguration : EntityTypeConfiguration<Cedulado>
    {
        public CeduladoEntityConfiguration()
        {
            ToTable("Cedulados");
            HasKey(c => new { c.SerieIdent, c.NumIdent, c.DVIdent });
            Ignore(c => c.Cedula);
            Property(c => c.SerieIdent).HasMaxLength(3).HasColumnName("MUN_CED");
            Property(c => c.NumIdent).HasMaxLength(7).HasColumnName("SEQ_CED");
            Property(c => c.DVIdent).HasMaxLength(1).HasColumnName("VER_CED");
            Property(c => c.PrimerApellido).HasColumnName("Apellido1");
            Property(c => c.SegundoApellido).HasColumnName("Apellido2").IsOptional();
            Property(c => c.FechaNacimiento).HasColumnName("Fecha_Nac");
            Property(c => c.LugarNacimiento).HasColumnName("Lugar_Nac");
            Property(c => c.TipoSanguinio).HasColumnName("COD_SANGRE").IsOptional();

            HasRequired(c => c.InformacionElectoral)
           .WithRequiredPrincipal();
        }
    }
}