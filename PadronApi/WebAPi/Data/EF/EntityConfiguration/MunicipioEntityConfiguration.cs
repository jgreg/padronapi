﻿using System.Data.Entity.ModelConfiguration;
using WebAPi.Domain;

namespace WebAPi.Data.EF.EntityConfiguration
{
    public class MunicipioEntityConfiguration : EntityTypeConfiguration<Municipio>
    {
        public MunicipioEntityConfiguration()
        {
            ToTable("Municipio");
            HasKey(m => m.MunicipioId);
            Property(m => m.MunicipioId).HasColumnName("Cod_Municipio").HasMaxLength(3);
            Property(m => m.Nombre).HasColumnName("Descripcion").HasMaxLength(35);

            //Relations
            HasMany(m => m.Recintos)
                .WithRequired(r => r.Municipio)
                .HasForeignKey(r => r.MunicipioId);

            HasMany(m => m.ColegiosElectorales)
                .WithRequired(ce => ce.Municipio)
                .HasForeignKey(r => r.MunicipioId);



        }
    }
}