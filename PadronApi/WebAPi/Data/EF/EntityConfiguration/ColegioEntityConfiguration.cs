﻿using System.Data.Entity.ModelConfiguration;
using WebAPi.Domain;

namespace WebAPi.Data.EF.EntityConfiguration
{
    public class ColegioEntityConfiguration : EntityTypeConfiguration<ColegioElectoral>
    {
        public ColegioEntityConfiguration()
        {
            ToTable("Colegios");
            HasKey(ce => new {ce.ColegioElectoralId, ce.MunicipioId});
            Property(ce => ce.ColegioElectoralId).HasColumnName("Colegio").HasMaxLength(6);
            Property(ce => ce.MunicipioId).HasColumnName("Cod_Municipio").HasMaxLength(3);
            Property(ce => ce.Nombre).HasColumnName("Descripcion").HasMaxLength(60);

        }
    }
}
