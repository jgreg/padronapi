﻿using System.Data.Entity;
using System.Reflection;
using WebAPi.Data.EF.EntityConfiguration;
using WebAPi.Domain;

namespace WebAPi.Data.EF
{
    public class PadronContext : DbContext
    {
        public PadronContext():base("name=PadronDbConnectionString")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            //modelBuilder.Configurations.Add(new CeduladoEntityConfiguration());
            //modelBuilder.Configurations.Add(new ColegioEntityConfiguration());
            //modelBuilder.Configurations.Add(new InformacionElectoralEntityConfiguration());
            //modelBuilder.Configurations.Add(new RecintoEntityConfiguration());
            
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetAssembly(GetType())); //current assembly


            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Cedulado> Cedulados { get; set; }
        public DbSet<InformacionElectoral> InformacionesElectoral { get; set; }
    }
}
