﻿namespace WebAPi.Domain
{
    public class ColegioElectoral
    {
       
        public string ColegioElectoralId { get; set; }
        public string MunicipioId { get; set; }
        public Municipio Municipio { get; set; }
        public string RecintoId { get; set; }
        public Recinto Recinto { get; set; }
        public string Nombre { get; set; }

    
    }
}
