﻿namespace WebAPi.Domain.Enums
{
    public enum TipoDeSangre
    {
        APositivo = 1,
        ANegativo = 2,
        BPositivo = 3,
        BNegativo = 4,
        AbPositivo = 5,
        AbNegativo = 6,
        OPositivo = 7,
        ONegativo = 8,
        NoDisponible = 9
    }
}