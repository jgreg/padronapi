﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainEntities.Enums;

namespace DomainEntities
{
    public class Cedulado
    {
        public string SerieIdent { get; set; }
        public string NumIdent { get; set; }
        public string DVIdent { get; set; }
        public string Nombres { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string LugarNacimiento { get; set; }
        public InformacionElectoral InformacionElectoral { get; set; }
        public string Sexo { get; set; }
        public TipoDeSangre TipoSanguinio { get; set; }


        public string Cedula
        {
            get { return string.Format("{0}{1}{2}", SerieIdent, NumIdent, DVIdent); }
        }
    }
}
