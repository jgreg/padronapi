using System.Collections.Generic;

namespace DomainEntities
{
    public class Municipio
    {
        public string MunicipioId { get; set; }
        public string Nombre { get; set; }

        public ICollection<Recinto> Recintos { get; set; }
        public ICollection<ColegioElectoral> ColegiosElectorales { get; set; }
    }
}