﻿namespace DomainEntities
{
    public class Recinto
    {
        public string RecintoId { get; set; }
        public string MunicipioId { get; set; }
        public Municipio Municipio { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public int? Capacidad { get; set; }
    }
}