﻿namespace DomainEntities
{
    public class InformacionElectoral
    {
        public string SerieIdent { get; set; }
        public string NumIdent { get; set; }
        public string DVIdent { get; set; }
        public string MunicipioId { get; set; }
        public Municipio Municipio { get; set; }
        public string RecintoId { get; set; }
        public Recinto Recinto { get; set; }
        public string ColegioElectoralId { get; set; }
        public ColegioElectoral ColegioElectoral { get; set; }
    }
}